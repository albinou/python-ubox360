from enum import Enum, auto


class AbsPosition(Enum):
    MIN = auto()
    CENTER = auto()
    MAX = auto()
