# `gamecontroller`

This program (part of the SDL project) generates a `gamecontrollerdb.txt` file with the gamepad mappings.

## Installation

ArchLinux users can install it from [AUR](https://aur.archlinux.org/packages/controllermap).
Other users need to build this [controllermap.c](http://hg.libsdl.org/SDL/raw-file/tip/test/controllermap.c) file.

## Generation of the `gamecontrollerdb.txt` file

Run this command and follow instructions:

    controllermap 0 > gamecontrollerdb.txt

This saves mappings into the `gamecontrollerdb.txt` file.
You can check your mappings with [sdl2-jstest](https://github.com/Grumbel/sdl-jstest) (also available on [AUR](https://aur.archlinux.org/packages/sdl2-jstest-git)) by running this command:

    sdl2-jstest --gamecontroller 0

## Play SDL games

If playing an SDL game, just copy your `gamecontrollerdb.txt` file to the root directory of your game.
